# import logging
# from logging.handlers import RotatingFileHandler

from waitress import serve
from slack_dns_manager import create_app
from config import Config

app = create_app()

if __name__ == '__main__':
    # handler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=1)
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # handler.setFormatter(formatter)

    if Config.DEBUG_ENABLED == 'True':
        # handler.setLevel(logging.DEBUG)
        # app.logger.addHandler(handler)
        app.run(debug=True, host=Config.HOST, port=Config.PORT)

    else:
        # handler.setLevel(logging.DEBUG)
        # app.logger.addHandler(handler)

        HOST = Config.HOST
        PORT = Config.PORT
        serve(app, host=HOST, port=PORT)
