from os import getenv


class Config:
    # app
    DEBUG_ENABLED = getenv('DEBUG_ENABLED', False)
    HOST = getenv('APP_SERVER_HOST', '0.0.0.0')
    PORT = getenv('APP_SERVER_PORT', '6030')
    RECORD_TYPES = ['A', 'CNAME', 'TXT']

    # slack
    VERIFICATION_TOKEN = getenv('VERIFICATION_TOKEN', 'Z4rQsm5JmPm2M')
    channels_chk = [i.split(",") for i in getenv('CHANNELS', 'testing').split(", ")]
    CHANNELS = [_channels[0] for _channels in channels_chk]
    WORKSPACE = getenv('WORKSPACE', 'example').lower()

    # godaddy
    GODADDY_KEY = getenv('GODADDY_KEY', 'abc123')
    GODADDY_SECRET_KEY = getenv('GODADDY_SECRET_KEY', 'qwerty123')

    # db
    DB_HOST = getenv('DB_HOST', 'localhost')
    DB = getenv('DATABASE', 'dnsmanager')
    DB_USER = getenv('DB_USER', 'dnsmanager')
    DB_PASSWORD = getenv('DB_PASSWORD', 'password')
    SQLALCHEMY_DATABASE_URI = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:5432/{DB}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
