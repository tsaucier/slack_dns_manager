# Slack DNS Manager  

## Getting started  
This project is currently configured to work with PostgreSQL, you will need to have the database created and ready to 
connect to, the app will create the tables.  

There are some environment variables required to use the application. The following table has all of the configurable 
environment variables the application can use.    

| Variable            | Description                                                              | Required | 
| :------------------ | :----------------------------------------------------------------------- | :------- | 
| DEBUG_ENABLED       | Boolean value, defaults to False. DO NOT run in a production environment | No       | 
| APP_SERVER_HOST     | The host IP address of which the application is running                  | No       |
| APP_SERVER_PORT     | The host port of which the application is running, defaults to 6030      | No       |       
| VERIFICATION_TOKEN  | Obtained through Slack once you create the app                           | Yes      |
| CHANNELS            | Channel or list of channels to whitelist, i.e. 'one, two, three'         | Yes      |
| WORKSPACE           | The Slack workspace in which the app will be used                        | Yes      |
| GODADDY_KEY         | API Credential key                                                       | Yes      |
| GODADDY_SECRET_KEY  | API Credential secret key                                                | Yes      |
| DB_HOST             | Host where the PostgreSQL instance is running                            | Yes      |
| DATABASE            | The name of the database                                                 | Yes      |
| DB_USER             | The user to connect to the database                                      | Yes      |
| DB_PASSWORD         | The password to connect to the database                                  | Yes      |

Once you have the database up and running, can start the container as such:
  
```
docker run -d \
-e VERIFICATION_TOKEN=qwerty123 \
-e CHANNELS=one, two, three \
-e WORKSPACE=example \
-e GODADDY_KEY=abc123 \
-e GODADDY_SECRET_KEY=abc1234 \
-e DB_HOST=localhost \
-e DATABASE=dnsmanager \
-e DB_USER=dnsmanager \
-e DB_PASSWORD=password \
registry.gitlab.com/tsaucier/slack_dns_manager:latest
```
Alternatively, you can modify the `docker-compose.yml` to your specifications, this will include PostgreSQL and the application.    

## Slack Setup  
Navigate to https://api.slack.com/apps to set up a new app.  
If using GoDaddy, you will need to create an [API Key](https://developer.godaddy.com/keys) then set it as an environment 
variable.  

## Slash commands
**GoDaddy**  

- `/gdget`: This will retrieve a record, if it exists. The format is`<name> <domain> <record_type>`.  
  - `record_type` is an optional parameter.  
- `/gdadd`: This will add a record. The format is `<value> <name> <domain> <record_type> <ttl>`.  
  - `value` is the IP address or the CNAME record (`10.20.30.40` or `test.example.com`) 
- `/update`: This will update a record. The format is `<value> <name> <domain> <record_type> <ttl>`.
  - This command is reserved for updating the `value` and `ttl` fields. The new `value` has to match its respective `record_type`. 
    Any other attempt will return an error. Example:  
    ```
    /gdupdate 12.34.56.78 test example.com A 3600 # record_type A
    /gdupdate developers.google.com test example.com CNAME 3600 # record_type CNAME
    ```
**Note:** GoDaddy does not support a `Delete` method via their API. If you need to remove a record, you will need to do so manually.  

## Database  
This application will require a database named `dnsmanager` to write and query against. All of my testing was on PostgreSQL.  

In order for the application to connect to postgresql, you will have to install a couple of packages. The following was ran against Ubuntu 18.04.  
`sudo apt-get install libpq-dev`  
`pip install psycopg2 libpq-dev`  

On startup, the application will create the tables (as long as there is a database to connect to):  

| Tables       | Description                                                           |
| :----------- | :-------------------------------------------------------------------- |
| record       | Every api call to the application logs the record that was queried    |
| user         | User information created from Slack information                       |
| userHistory  | Logs the history of commands made by a user                           |

All users that have access to the commands are assumed to be active if they are part of the whitelisted channels located in the config.  
Managers of the application have the ability to update the `user` table `active` column boolean value if certain users in the channel are forbidden to make changes.  

## Testing  
The easiest way to test is to install [postman](https://www.getpostman.com/downloads/).  

Once Postman is installed, You can import the collections from [s3](https://travissaucier.s3.us-east-2.amazonaws.com/cdn/Collections-20200515T035658Z-001.zip).  

The `docker-compose.yml` has a PostgreSQL image. To debug locally using the image, you can bring it up by running `docker-compose up -d db` then adding the connection information to `config.py`  
