FROM python:3.7-alpine

EXPOSE 6030

ENV PYTHONUNBUFFERED=1

RUN mkdir -p /opt/slack_dns_manager

WORKDIR /opt/slack_dns_manager

ADD slack_dns_manager slack_dns_manager
ADD config.py .
ADD requirements.txt .
ADD run.py .

RUN apk update && \
    apk add postgresql-dev gcc python3-dev musl-dev && \
    pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["python", "run.py"]