from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import Config

db = SQLAlchemy()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)

    from slack_dns_manager.godaddy.routes import godaddy
    from slack_dns_manager.errors.handlers import errors

    app.register_blueprint(godaddy)
    app.register_blueprint(errors)

    with app.app_context():
        from slack_dns_manager.godaddy import routes
        db.create_all()

        return app
