from flask import Blueprint, render_template


errors = Blueprint('errors', __name__)


@errors.app_errorhandler(400)
def error_400(error):
    return render_template('errors/400.json'), 400


@errors.app_errorhandler(401)
def error_401(error):
    return render_template('errors/401.json'), 401


@errors.app_errorhandler(404)
def error_404(error):
    return render_template('errors/404.json'), 404


@errors.app_errorhandler(409)
def error_409(error):
    return render_template('errors/409.json'), 409
