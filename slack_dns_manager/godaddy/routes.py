from flask import request, Blueprint
from slack_dns_manager.security import authenticate
from slack_dns_manager.slack.forms import FormData

godaddy = Blueprint('godaddy', __name__)


@godaddy.route('/api/gd', methods=['POST'])
def gd():
    form = FormData(request.form)
    authenticated = authenticate(form.get_slack_user_id(), form.get_auth_token(), form.get_channel(),
                                 form.get_workspace())

    if authenticated:
        if form.get_command() == '/gdget':
            from .gdget import get
            return get(form)

        if form.get_command() == '/gdadd':
            from .gdadd import add
            return add(form)

        if form.get_command() == '/gdupdate':
            from .gdupdate import update
            return update(form)

    else:
        return f'You are not authorized to use this command!'
