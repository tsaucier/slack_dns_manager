import requests
from flask import jsonify
from slack_dns_manager.slack.user import user_data, user_history
from slack_dns_manager.godaddy.records import gdget_data
from config import Config


def get(form):
    contents = form.get_text().split()
    headers = {'Authorization': f'sso-key {Config.GODADDY_KEY}:{Config.GODADDY_SECRET_KEY}'}
    name = contents[0].lower()
    domain = contents[1].lower()

    if len(contents) < 3:
        for record_type in Config.RECORD_TYPES:
            url = f'https://api.godaddy.com/v1/domains/{domain}/records/{record_type}/{name}'
            req = requests.get(url, headers=headers).json()

            user_data(form)
            if req:
                data = gdget_data(req, domain, form)
                user_history(form, data)
                return jsonify({"response_type": "in_channel", "text": str(req).strip('{}[]')})
        else:
            return 'Not Found'

    else:
        record_type = contents[2].upper()
        url = f'https://api.godaddy.com/v1/domains/{domain}/records/{record_type}/{name}'
        req = requests.get(url, headers=headers).json()

        if req:
            data = gdget_data(req, domain, form)
            user_history(form, data)
            return jsonify({"response_type": "in_channel", "text": str(req).strip('{}[]')})
        else:
            return 'Not Found'
