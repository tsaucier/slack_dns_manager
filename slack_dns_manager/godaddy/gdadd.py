import requests
from slack_dns_manager.slack.user import user_data, user_history
from slack_dns_manager.godaddy.records import gdadd_data
from config import Config


def add(form):
    contents = form.get_text().split()
    headers = {'Authorization': f'sso-key {Config.GODADDY_KEY}:{Config.GODADDY_SECRET_KEY}',
               'Content-Type': 'application/json'}
    name = contents[1].lower()
    data = f'"data": "{contents[0].lower()}", ' \
           f'"name": "{contents[1].lower()}", ' \
           f'"type": "{contents[3].upper()}", ' \
           f'"ttl": {contents[4]}'
    data = str([{data}]).replace("'", "")
    domain = contents[2].lower()

    user_data(form)

    for record_type in Config.RECORD_TYPES:
        url = f'https://api.godaddy.com/v1/domains/{domain}/records/{record_type}/{name}'
        record_check = requests.get(url, headers=headers)
        if not record_check == 200:
            url = f'https://api.godaddy.com/v1/domains/{domain}/records'
            req = requests.patch(url, headers=headers, data=data)
            if req.status_code == 200:
                data = gdadd_data(domain, form)
                user_history(form, data)
                return 'Success!', req.status_code
            else:
                resp = req.json()
                return resp['message'], req.status_code
        else:
            return 'Does not exist'
