from slack_dns_manager import db
from slack_dns_manager.models import Record


def gdget_data(req, domain, form):
    record = Record(value=str(req[0]['data']),
                    name=str(req[0]['name']),
                    domain=domain,
                    recordType=str(req[0]['type']).upper(),
                    ttl=str(req[0]['ttl']),
                    command=form.get_command())
    db.session.add(record)
    db.session.commit()

    return record


def gdadd_data(domain, form):
    contents = form.get_text().split()
    record = Record(value=contents[0].lower(),
                    name=contents[1].lower(),
                    domain=domain,
                    recordType=contents[3].upper(),
                    ttl=contents[4],
                    command=form.get_command())
    db.session.add(record)
    db.session.commit()

    return record


def gdupdate_data(domain, form):
    contents = form.get_text().split()
    record = Record(value=contents[0].lower(),
                    name=contents[1].lower(),
                    domain=domain,
                    recordType=contents[3].upper(),
                    ttl=contents[4],
                    command=form.get_command())
    db.session.add(record)
    db.session.commit()

    return record
