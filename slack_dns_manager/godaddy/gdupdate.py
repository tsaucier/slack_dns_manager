import requests
from slack_dns_manager.slack.user import user_data, user_history
from slack_dns_manager.godaddy.records import gdadd_data
from config import Config


def update(form):
    contents = form.get_text().split()
    headers = {'Authorization': f'sso-key {Config.GODADDY_KEY}:{Config.GODADDY_SECRET_KEY}',
               'Content-Type': 'application/json'}
    value = contents[0].lower()
    name = contents[1].lower()
    domain = contents[2].lower()
    record_type = contents[3].upper()
    ttl = contents[4]
    url = f'https://api.godaddy.com/v1/domains/{domain}/records/{record_type}/{name}'
    data = f'"data": "{value}",' \
           f'"ttl": {ttl}'
    data = str([{data}]).replace("'", "")

    user_data(form)

    record_check = requests.get(url, headers=headers).json()
    if record_check:
        req = requests.put(url, headers=headers, data=data)

        if req.status_code == 200:
            data = gdadd_data(domain, form)
            user_history(form, data)
            return 'Success!', req.status_code
        else:
            resp = req.json()
            return resp['fields'][0]['message'], req.status_code
    else:
        return 'That record was not found. Unable to update!', 404
