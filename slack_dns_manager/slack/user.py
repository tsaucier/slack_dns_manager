from slack_dns_manager import db
from slack_dns_manager.models import User, UserHistory


def user_data(form):
    if not User.query.filter_by(slackUserId=form.get_slack_user_id()).first():
        user = User(slackUserId=form.get_slack_user_id(),
                    slackUserName=form.get_slack_user_name())
        db.session.add(user)
        db.session.commit()


def user_history(form, record):
    slack_user_id = User.query.filter_by(slackUserId=form.get_slack_user_id()).first()
    history = UserHistory(userId=slack_user_id.id,
                          recordId=record.id,
                          command=form.get_command())
    db.session.add(history)
    db.session.commit()
