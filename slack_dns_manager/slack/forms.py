class FormData:

    def __init__(self, form):
        self.form = form

    def get_auth_token(self):
        return self.form['token']

    def get_workspace(self):
        return self.form['team_domain'].lower()

    def get_channel(self):
        return self.form['channel_name']

    def get_slack_user_id(self):
        return self.form['user_id']

    def get_slack_user_name(self):
        return self.form['user_name']

    def get_command(self):
        return self.form['command']

    def get_text(self):
        return self.form['text']
