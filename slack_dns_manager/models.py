from datetime import datetime
from slack_dns_manager import db


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    slackUserId = db.Column(db.String(), unique=True, nullable=False, )
    slackUserName = db.Column(db.String(), unique=True, nullable=True)
    active = db.Column(db.Boolean(), nullable=False, default=True)

    def __repr__(self):
        return f'User("{self.slackUserId}", "{self.slackUserName}", "{self.active}")'


class Record(db.Model):
    __tablename__ = 'record'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(), unique=False, nullable=False)
    name = db.Column(db.String(), unique=False, nullable=False)
    domain = db.Column(db.String(), unique=False, nullable=False)
    recordType = db.Column(db.String(), unique=False, nullable=False)
    ttl = db.Column(db.Integer(), unique=False, nullable=False)
    command = db.Column(db.String(), unique=False, nullable=False)

    def __repr__(self):
        return f'Record({self.value}", "{self.name}", "{self.domain}", ' \
               f'"{self.recordType}", "{self.ttl}", "{self.command}")'


class UserHistory(db.Model):
    __tablename__ = 'userHistory'

    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer(), db.ForeignKey('user.id'), nullable=False)
    recordId = db.Column(db.Integer(), db.ForeignKey('record.id'), nullable=True)
    command = db.Column(db.String(), unique=False, nullable=False)
    commandTimeUTC = db.Column(db.DateTime(), unique=False, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f'UserHistory({self.userId}", "{self.recordId}", "{self.command}", ' \
               f'"{self.commandTimeUTC}"'
