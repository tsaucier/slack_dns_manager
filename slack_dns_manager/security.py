from slack_dns_manager.models import User
from config import Config


def authenticate(slack_user_id, token, channel, workspace):

    def is_active():
        user = User.query.filter_by(slackUserId=slack_user_id).first()
        if not user:
            return True
        else:
            return user.active

    rules = [
        is_active(),
        token == Config.VERIFICATION_TOKEN,
        channel in Config.CHANNELS,
        workspace == Config.WORKSPACE
    ]
    if all(rules):
        return True
    else:
        return False
